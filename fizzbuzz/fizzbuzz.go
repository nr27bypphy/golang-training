package fizzbuzz

import "strconv"

type FizzBuzz struct {}

func (f FizzBuzz) convert(input int) string {
	if input % 15 == 0 {
		return "fizzbuzz"
	}
	if input % 5 == 0 {
		return "buzz"
	}
	if input % 3 == 0 {
		return "fizz"
	}
	return strconv.Itoa(input)
}
