package fizzbuzz

import (
	"gitlab.com/nr27bypphy/golang-training/tests"
	"testing"
)

func TestFizzBuzz(t *testing.T) {
	t.Run("1の時1を返す", func(t *testing.T) {
		expected := "1"
		input := 1
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("2の時2を返す", func(t *testing.T) {
		expected := "2"
		input := 2
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("3の時fizzを返す", func(t *testing.T) {
		expected := "fizz"
		input := 3
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("6の時fizzを返す", func(t *testing.T) {
		expected := "fizz"
		input := 6
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("5の時buzzを返す", func(t *testing.T) {
		expected := "buzz"
		input := 5
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("10の時buzzを返す", func(t *testing.T) {
		expected := "buzz"
		input := 10
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("15の時fizzbuzzを返す", func(t *testing.T) {
		expected := "fizzbuzz"
		input := 15
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
	t.Run("30の時fizzbuzzを返す", func(t *testing.T) {
		expected := "fizzbuzz"
		input := 30
		actual := new(FizzBuzz).convert(input)
		tests.Assert(expected, actual, t)
	})
}

