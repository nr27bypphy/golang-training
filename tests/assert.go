package tests

import (
	"runtime"
	"testing"
)

func Assert(expected string, actual string, t *testing.T) {
	if actual != expected {
		_, file, line, ok := runtime.Caller(1) // 1は、この関数の直接の呼び出し元を意味します
		if !ok {
			t.Errorf("Error retrieving caller info")
		}
		t.Errorf("\nFile: %s, Line: %d\nExpected: %s\nActual: %s", file, line, expected, actual)
	}
}
