# golang-training
## what is this?
- golangのトレーニング用

## planning
- [ ] all code is built by TDD
  - [x] fizzbuzz(基本構文)
  - [x] vending machine
  - [ ] simple api
  - [ ] api with store

## history
- create first main.go
  - fmt.println
- create fizzbuzz
- understand what package is
  - to import another package, first letter must be capitalized
- create struct and methods
- create vendingmachine(first acceptance test)
  - to update instance variable, set pointer receiver
    - I cannot set both value and pointer receivers in one struct?