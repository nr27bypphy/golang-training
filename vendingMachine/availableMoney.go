package vendingMachine

type AvailableMoney struct {
	value map[int]bool
}

func newAvailableMoney(value map[int]bool) AvailableMoney {
	return AvailableMoney{
		value: value,
	}
}

func (am AvailableMoney) exists(money int) bool {
	return am.value[money]
}