package vendingMachine

import (
	"gitlab.com/nr27bypphy/golang-training/tests"
	"strconv"
	"testing"
)

// 入金できる
// 	10円、50円、100円、500円、1000円
// 補充できる
//　商品はコーラ（120円）と水（100円）
// 購入できる
//　お釣りを受け取れる

func TestVendingMachine(t *testing.T) {
	t.Run("購入できる", func(t *testing.T) {
		t.Run("水を購入できる", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "Water", t)
			change := vm.change()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(change), t)
		})
		t.Run("水を購入すると投入額から100円引かれた値のお釣りが返却されること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			vm.insert(100)
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "Water", t)
			change := vm.change()
			tests.Assert(strconv.Itoa(100), strconv.Itoa(change), t)
		})
		t.Run("コーラを購入できる", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Cola")
			vm.insert(100)
			vm.insert(10)
			vm.insert(10)
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "Cola", t)
			change := vm.change()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(change), t)
		})
		t.Run("コーラを購入すると投入額から120円引かれた値のお釣りが返却されること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Cola")
			vm.insert(100)
			vm.insert(100)
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "Cola", t)
			change := vm.change()
			tests.Assert(strconv.Itoa(80), strconv.Itoa(change), t)
		})
		t.Run("水を2回購入できる", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			vm.putDrink("Water")
			vm.insert(100)
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "Water", t)
			product = vm.buy(input)
			tests.Assert(product, "Water", t)
			change := vm.change()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(change), t)
		})
		t.Run("水を購入すると投入額が100円減ること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			vm.insert(100)
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "Water", t)
			amount := vm.getAmount()
			tests.Assert(strconv.Itoa(100), strconv.Itoa(amount), t)
		})
		t.Run("コーラを購入すると投入額が120円減ること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Cola")
			vm.insert(100)
			vm.insert(100)
			vm.insert(10)
			vm.insert(10)
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "Cola", t)
			amount := vm.getAmount()
			tests.Assert(strconv.Itoa(100), strconv.Itoa(amount), t)
		})
		t.Run("投入額が100円未満の時水を購入できないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "", t)
			amount := vm.getAmount()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(amount), t)
		})
		t.Run("投入額が120円未満の時コーラを購入できないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Cola")
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "", t)
			amount := vm.getAmount()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(amount), t)
		})
		t.Run("水を購入すると水の在庫が1つ減ること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Water")
			vm.putDrink("Water")
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "Water", t)
			stock := vm.getStock(input)
			tests.Assert(strconv.Itoa(1), strconv.Itoa(stock), t)
		})
		t.Run("コーラを購入するとコーラの在庫が1つ減ること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.putDrink("Cola")
			vm.putDrink("Cola")
			vm.insert(100)
			vm.insert(10)
			vm.insert(10)
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "Cola", t)
			stock := vm.getStock(input)
			tests.Assert(strconv.Itoa(1), strconv.Itoa(stock), t)
		})
		t.Run("水の在庫がない時、水を購入できないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.insert(100)
			input := "Water"
			product := vm.buy(input)
			tests.Assert(product, "", t)
			stock := vm.getStock(input)
			tests.Assert(strconv.Itoa(0), strconv.Itoa(stock), t)
		})
		t.Run("コーラの在庫がない時、コーラを購入できないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.insert(100)
			vm.insert(10)
			vm.insert(10)
			input := "Cola"
			product := vm.buy(input)
			tests.Assert(product, "", t)
			stock := vm.getStock(input)
			tests.Assert(strconv.Itoa(0), strconv.Itoa(stock), t)
		})
	})
	t.Run("ドリンクの補充", func(t *testing.T) {
		t.Run("水を補充できる", func(t *testing.T) {
			vm := NewVendingMachine()
			drink := "Water"
			stockResult := vm.putDrink(drink)
			tests.Assert(strconv.FormatBool(true), strconv.FormatBool(stockResult), t)
			numberOfStock := vm.getStock(drink)
			tests.Assert(strconv.Itoa(1), strconv.Itoa(numberOfStock), t)
		})
		t.Run("水を2回補充できる", func(t *testing.T) {
			vm := NewVendingMachine()
			drink := "Water"
			vm.putDrink(drink)
			stockResult := vm.putDrink(drink)
			tests.Assert(strconv.FormatBool(true), strconv.FormatBool(stockResult), t)
			numberOfStock := vm.getStock(drink)
			tests.Assert(strconv.Itoa(2), strconv.Itoa(numberOfStock), t)
		})
		t.Run("水を補充しないと残りの水はないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			numberOfStock := vm.getStock("Water")
			tests.Assert(strconv.Itoa(0), strconv.Itoa(numberOfStock), t)
		})
		t.Run("コーラを補充できること", func(t *testing.T) {
			vm := NewVendingMachine()
			drink := "Cola"
			stockResult := vm.putDrink(drink)
			tests.Assert(strconv.FormatBool(true), strconv.FormatBool(stockResult), t)
			numberOfStock := vm.getStock(drink)
			tests.Assert(strconv.Itoa(1), strconv.Itoa(numberOfStock), t)
		})
		t.Run("コーラを補充した後、水のストックは補充していないので0であること", func(t *testing.T) {
			vm := NewVendingMachine()
			drink := "Cola"
			stockResult := vm.putDrink(drink)
			tests.Assert(strconv.FormatBool(true), strconv.FormatBool(stockResult), t)
			numberOfStock := vm.getStock("Water")
			tests.Assert(strconv.Itoa(0), strconv.Itoa(numberOfStock), t)
		})
		t.Run("コーラと水以外は補充できないこと", func(t *testing.T) {
			vm := NewVendingMachine()
			drink := "Tea"
			stockResult := vm.putDrink(drink)
			tests.Assert(strconv.FormatBool(false), strconv.FormatBool(stockResult), t)
			numberOfStock := vm.getStock("Water")
			tests.Assert(strconv.Itoa(0), strconv.Itoa(numberOfStock), t)
		})
	})
	t.Run("お金の投入", func(t *testing.T) {
		var cases = []struct {
			name   string // テストケースの名前
			money  int    // 入力値
			result bool
			expect int // 期待される結果
		}{
			{"1円は投入できないこと", 1, false, 0},
			{"5円は投入できないこと", 5, false, 0},
			{"2000円は投入できないこと", 2000, false, 0},
			{"5000円は投入できないこと", 5000, false, 0},
			{"10000円は投入できないこと", 10000, false, 0},
			{"10円を投入すると10円が入金されていること", 10, true, 10},
			{"50円を投入すると50円が入金されていること", 50, true, 50},
			{"100円を投入すると100円が入金されていること", 100, true, 100},
			{"500円を投入すると500円が入金されていること", 500, true, 500},
			{"1000円を投入すると1000円が入金されていること", 1000, true, 1000},
			{"10円,50円,100円,500円,1000円以外は投入できないこと", 123, false, 0},
		}

		for _, tt := range cases {
			t.Run(tt.name, func(t *testing.T) {
				vm := NewVendingMachine()
				insertResult := vm.insert(tt.money)
				tests.Assert(strconv.FormatBool(tt.result), strconv.FormatBool(insertResult), t)
				totalOfMoney := vm.getAmount()
				tests.Assert(strconv.Itoa(tt.expect), strconv.Itoa(totalOfMoney), t)
			})
		}
		t.Run("入金しないと投入額は0円であること", func(t *testing.T) {
			vm := NewVendingMachine()
			totalOfMoney := vm.getAmount()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(totalOfMoney), t)
		})
		t.Run("2回投入できること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.insert(100)
			insertResult := vm.insert(100)
			tests.Assert(strconv.FormatBool(true), strconv.FormatBool(insertResult), t)
			totalOfMoney := vm.getAmount()
			tests.Assert(strconv.Itoa(200), strconv.Itoa(totalOfMoney), t)
		})
	})
	t.Run("お釣りの返却", func(t *testing.T) {
		t.Run("100円入れてお釣りをとると、投入額が0になること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.insert(100)
			change := vm.change()
			tests.Assert(strconv.Itoa(100), strconv.Itoa(change), t)
			totalOfMoney := vm.getAmount()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(totalOfMoney), t)
		})
		t.Run("200円入れてお釣りをとると、投入額が0になること", func(t *testing.T) {
			vm := NewVendingMachine()
			vm.insert(100)
			vm.insert(100)
			change := vm.change()
			tests.Assert(strconv.Itoa(200), strconv.Itoa(change), t)
			totalOfMoney := vm.getAmount()
			tests.Assert(strconv.Itoa(0), strconv.Itoa(totalOfMoney), t)
		})
	})
}
