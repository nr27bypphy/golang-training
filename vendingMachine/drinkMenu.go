package vendingMachine

type DrinkMenu struct {
	value map[string]int
}

func newDrinkMenu() DrinkMenu {
	return DrinkMenu{
		value: map[string]int{"Cola": 120, "Water": 100},
	}
}

func (dm DrinkMenu) has(drink string) bool {
	_, exists := dm.value[drink]
	return exists
}

func (dm DrinkMenu) price(drink string) int {
	return dm.value[drink]
}
