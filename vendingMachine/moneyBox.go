package vendingMachine

type MoneyBox struct {
	value int
}

func (mb *MoneyBox) take(price int) {
	mb.value -= price
}

func (mb *MoneyBox) hasEnough(price int) bool {
	return mb.value >= price
}

func (mb *MoneyBox) check() int {
	return mb.value
}

func (mb *MoneyBox) insert(money int) {
	mb.value += money
}

func (mb *MoneyBox) reset() {
	mb.value = 0
}

func newMoneyBox(value int) MoneyBox {
	return MoneyBox{
		value: value,
	}
}
