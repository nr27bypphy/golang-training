package vendingMachine

type DrinkBox struct {
	value map[string]int
}

func NewDrinkBox() DrinkBox {
	return DrinkBox{
		value: map[string]int{
			"Water": 0,
			"Cola":  0,
		},
	}
}

func (db *DrinkBox) take(drink string) {
	db.value[drink] -= 1
}

func (db *DrinkBox) put(drink string) {
	db.value[drink] += 1
}

func (db *DrinkBox) check(drink string) int {
	return db.value[drink]
}

func (db *DrinkBox) hasDrink(drink string) bool {
	return db.check(drink) > 0
}
