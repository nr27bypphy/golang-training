package vendingMachine

type VendingMachine struct {
	drinkBox DrinkBox
	moneyBox MoneyBox
	drinkMenu DrinkMenu
	availableMoney AvailableMoney
}

func NewVendingMachine() *VendingMachine {
	return &VendingMachine{
		drinkBox: NewDrinkBox(),
		moneyBox: newMoneyBox(0),
		drinkMenu: newDrinkMenu(),
		availableMoney: newAvailableMoney(map[int]bool{10: true, 50: true, 100: true, 500: true, 1000: true}),
	}
}

func (vm *VendingMachine) putDrink(drink string) bool {
	if vm.drinkMenu.has(drink) {
		vm.drinkBox.put(drink)
		return true
	}
	return false
}

func (vm *VendingMachine) insert(money int) bool {
	if vm.availableMoney.exists(money) {
		vm.moneyBox.insert(money)
		return true
	}
	return false
}

func (vm *VendingMachine) buy(drink string) string {
	price := vm.drinkMenu.price(drink)
	if vm.isAvailable(drink, price) {
		vm.moneyBox.take(price)
		vm.drinkBox.take(drink)
		return drink
	}
	return ""
}

func (vm *VendingMachine) isAvailable(drink string, price int) bool {
	return vm.moneyBox.hasEnough(price) && vm.drinkBox.hasDrink(drink)
}

func (vm *VendingMachine) change() int {
	totalAmount := vm.moneyBox.check()
	vm.moneyBox.reset()
	return totalAmount
}

func (vm *VendingMachine) getStock(drink string) int {
	return vm.drinkBox.check(drink)
}

func (vm *VendingMachine) getAmount() int {
	return vm.moneyBox.value
}
